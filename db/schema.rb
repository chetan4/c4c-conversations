# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140411074655) do

  create_table "api_requests", :force => true do |t|
    t.text     "keywords"
    t.integer  "api_response_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "bucket"
    t.string   "publisher"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.string   "request_id"
    t.string   "request_type"
    t.integer  "search_request_count",      :default => 0
    t.integer  "rehydration_request_count", :default => 0
    t.string   "status",                    :default => "REQUESTED"
  end

  add_index "api_requests", ["api_response_id"], :name => "index_api_requests_on_api_response_id"
  add_index "api_requests", ["request_id"], :name => "index_api_requests_on_request_id"

  create_table "conversations", :force => true do |t|
    t.integer  "root_tweet_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "checksum"
    t.integer  "api_request_id"
  end

  add_index "conversations", ["api_request_id"], :name => "index_conversations_on_api_request_id"
  add_index "conversations", ["checksum"], :name => "index_conversations_on_checksum"

  create_table "hash_tags", :force => true do |t|
    t.string   "tag"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tweet_conversations", :force => true do |t|
    t.integer  "conversation_id"
    t.integer  "tweet_id"
    t.integer  "position"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "tweet_conversations", ["conversation_id"], :name => "index_tweet_conversations_on_conversation_id"
  add_index "tweet_conversations", ["tweet_id"], :name => "index_tweet_conversations_on_tweet_id"

  create_table "tweet_hash_tags", :force => true do |t|
    t.integer  "hash_tag_id"
    t.integer  "tweet_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "tweet_hash_tags", ["hash_tag_id", "tweet_id"], :name => "index_tweet_hash_tags_on_hash_tag_id_and_tweet_id"

  create_table "tweet_tweet_urls", :force => true do |t|
    t.integer  "tweet_id"
    t.integer  "tweet_url_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "tweet_tweet_urls", ["tweet_id", "tweet_url_id"], :name => "index_tweet_tweet_urls_on_tweet_id_and_tweet_url_id"

  create_table "tweet_urls", :force => true do |t|
    t.string   "short_url"
    t.text     "expanded_url"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "tweets", :force => true do |t|
    t.string   "body"
    t.string   "link"
    t.datetime "posted_at"
    t.integer  "favorites_count",      :default => 0
    t.integer  "retweet_count",        :default => 0
    t.integer  "twitter_user_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "batch_id"
    t.datetime "batch_timestamp"
    t.string   "tweet_id"
    t.string   "in_reply_to_tweet_id"
  end

  add_index "tweets", ["batch_id"], :name => "index_tweets_on_batch_id"
  add_index "tweets", ["batch_timestamp"], :name => "index_tweets_on_batch_timestamp"
  add_index "tweets", ["in_reply_to_tweet_id"], :name => "index_tweets_on_in_reply_to_tweet_id"
  add_index "tweets", ["tweet_id"], :name => "index_tweets_on_tweet_id"

  create_table "twitter_users", :force => true do |t|
    t.string   "link"
    t.string   "display_name"
    t.string   "image_url"
    t.text     "summary"
    t.integer  "follower_count",     :default => 0
    t.integer  "statuses_count",     :default => 0
    t.string   "twitter_time_zone"
    t.string   "preferred_username"
    t.string   "location"
    t.integer  "favorites_count",    :default => 0
    t.text     "other_links"
    t.integer  "following_count",    :default => 0
    t.string   "entity_type"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
  end

end
