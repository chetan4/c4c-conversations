class CreateTweetHashTags < ActiveRecord::Migration
  def change
    create_table :tweet_hash_tags do |t|
      t.integer :hash_tag_id
      t.integer :tweet_id

      t.timestamps
    end

    add_index(:tweet_hash_tags, [:hash_tag_id, :tweet_id])
  end
end
