class AddSearchRequestCountAndRehydrationRequestCountToApiRequests < ActiveRecord::Migration
  def change
    add_column :api_requests, :search_request_count, :integer, :default => 0
    add_column :api_requests, :rehydration_request_count, :integer, :default => 0
  end
end
