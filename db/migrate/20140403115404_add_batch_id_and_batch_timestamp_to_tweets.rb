class AddBatchIdAndBatchTimestampToTweets < ActiveRecord::Migration
  def change
    add_column(:tweets, :batch_id, :string)
    add_column(:tweets, :batch_timestamp, :datetime)

    add_index(:tweets, :batch_id)
    add_index(:tweets, :batch_timestamp)
  end
end
