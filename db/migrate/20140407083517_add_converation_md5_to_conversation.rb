class AddConverationMd5ToConversation < ActiveRecord::Migration
  def change
    add_column :conversations, :checksum, :string
    add_index :conversations, :checksum
  end
end
