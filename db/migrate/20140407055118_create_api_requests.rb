class CreateApiRequests < ActiveRecord::Migration
  def change
    create_table :api_requests do |t|
      t.text :keywords
      t.integer :api_response_id
      t.datetime :start_date
      t.datetime :end_date
      t.string :bucket
      t.string :publisher

      t.timestamps
    end

    add_index(:api_requests, :api_response_id)
  end
end
