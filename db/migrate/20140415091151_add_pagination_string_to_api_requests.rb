class AddPaginationStringToApiRequests < ActiveRecord::Migration
  def change
    add_column :api_requests, :pagination, :text
  end
end
