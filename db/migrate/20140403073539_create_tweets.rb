class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.string :body
      t.string :link
      t.column :posted_at, :datetime
      t.integer :favorites_count, :default => 0
      t.integer :retweet_count, :default => 0
      t.integer :twitter_user_id

      t.timestamps
    end
  end
end
