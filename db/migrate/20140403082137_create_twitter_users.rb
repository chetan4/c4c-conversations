class CreateTwitterUsers < ActiveRecord::Migration
  def change
    create_table :twitter_users do |t|
      t.string :link
      t.string :display_name
      t.string :image_url
      t.text :summary
      t.integer :follower_count, :default => 0
      t.integer :statuses_count, :default => 0
      t.string :twitter_time_zone
      t.string :preferred_username
      t.string :location
      t.integer :favorites_count, :default => 0
      t.text :other_links
      t.integer :following_count, :default => 0
      t.string :entity_type

      t.timestamps
    end
  end
end
