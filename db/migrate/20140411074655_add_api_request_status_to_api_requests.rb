class AddApiRequestStatusToApiRequests < ActiveRecord::Migration
  def change
    add_column :api_requests, :status, :string, :default => 'REQUESTED'
  end
end
