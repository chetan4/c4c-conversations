class AddRequestIdAndRequestTypeToApiRequest < ActiveRecord::Migration
  def change
    add_column :api_requests, :request_id, :string
    add_column :api_requests, :request_type, :string

    add_index(:api_requests, :request_id)
  end
end
