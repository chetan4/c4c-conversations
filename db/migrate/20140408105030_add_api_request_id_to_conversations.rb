class AddApiRequestIdToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :api_request_id, :integer
    add_index(:conversations, :api_request_id)
  end
end
