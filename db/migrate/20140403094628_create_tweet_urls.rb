class CreateTweetUrls < ActiveRecord::Migration
  def change
    create_table :tweet_urls do |t|
      t.string :short_url
      t.text :expanded_url

      t.timestamps
    end
  end
end
