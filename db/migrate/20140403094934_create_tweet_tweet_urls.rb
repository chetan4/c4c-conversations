class CreateTweetTweetUrls < ActiveRecord::Migration
  def change
    create_table :tweet_tweet_urls do |t|
      t.integer :tweet_id
      t.integer :tweet_url_id

      t.timestamps
    end

    add_index(:tweet_tweet_urls, [:tweet_id, :tweet_url_id])
  end
end
