class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.integer :root_tweet_id
      t.timestamps
    end
  end
end
