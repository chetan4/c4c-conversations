class CreateTweetConverations < ActiveRecord::Migration
  def change
    create_table :tweet_conversations do |t|
      t.integer :conversation_id
      t.integer :tweet_id
      t.integer :position

      t.timestamps
    end

    add_index(:tweet_conversations, :conversation_id)
    add_index(:tweet_conversations, :tweet_id)
  end
end
