APP_ROOT = '/srv/apps/c4c-conversations'
SHARED_FOLDER = APP_ROOT + '/shared'
CONFIG_FOLDER = SHARED_FOLDER + '/config'
QUEUE_WORKERS = 'api_request_processor, conversation_updater, parent_tweet_worker'
COUNT = 2
TMP_FILE = SHARED_FOLDER + '/tmp'
ENVIRONMENT = 'staging'


COUNT.times do |index|
  i = index + 1
  God.watch do |w|
    w.log = "#{SHARED_FOLDER}/log/resque-workers-#{i}.log"
    w.name = "resque-workers=#{i}"
    w.start = "bundle exec rake environment resque:work QUEUE='#{QUEUE_WORKERS}'"
    w.stop = "kill -9 #{w.pid}"
    w.restart = "kill -9 #{w.pid} && bundle exec rake environment resque:work QUEUE='#{QUEUE_WORKERS}'"

    w.pid_file = File.join(TMP_FILE + '/pids', "resque-workers-#{i}.pid")

    w.behavior(:clean_pid_file)

    w.start_if do |start|
      start.condition(:process_running) do |c|
        c.interval = 5.seconds
        c.running = false
      end
    end

    w.restart_if do |restart|
      restart.condition(:memory_usage) do |c|
        c.interval = 30.seconds
        c.above = 600.megabytes
        c.times = [3, 5] # 3 out of 5 intervals
      end

      restart.condition(:cpu_usage) do |c|
        c.interval = 30.seconds
        c.above = 70.percent
        c.times = 5
      end
    end

    # lifecycle
    w.lifecycle do |on|
      on.condition(:flapping) do |c|
        c.to_state = [:start, :restart]
        c.times = 5
        c.within = 5.minute
        c.transition = :unmonitored
        c.retry_in = 10.minutes
        c.retry_times = 5
        c.retry_within = 2.hours
      end
    end
  end
end