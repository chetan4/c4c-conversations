REDIS_CONFIG = YAML.load_file("#{Rails.root}/config/redis.yml")[Rails.env.to_s]

$redis = Redis.new(:host => REDIS_CONFIG['host'], :port => REDIS_CONFIG['port'])

if defined?(PhusionPassenger)
  PhusionPassenger.on_event(:starting_worker_process) do |forked|
    if forked
      $redis.client.disconnect
      $redis = Redis.new(:host => REDIS_CONFIG['host'],
                         :port => REDIS_CONFIG['port'])
      Rails.logger.info "Reconnecting to redis"
    else
      # We're in conservative spawning mode. We don't need to do anything.
    end
  end
end


REDIS_CONVERSATIONS_FOR_HQ = 'conversations_for_hq'

