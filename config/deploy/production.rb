set :stage, :production

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
role :app, %w{deploy@example.com}
role :web, %w{deploy@example.com}
role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.
server 'example.com', user: 'deploy', roles: %w{web app}, my_property: :my_value

namespace :deploy do
  namespace :deploy do
    task :copy_god_files do
      on roles(:app) do
        execute 'cp /srv/apps/c4c-conversations/current/config/production_god.rb /srv/apps/c4c-conversations/shared/config/.'
      end
    end

    task :stop_god do
      on roles(:app) do
        execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec god stop'
      end
    end

    task :restart_god do
      on roles(:app) do
        #execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec god stop'
        execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec god stop'
        execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec god load /srv/apps/c4c-conversations/shared/config/production_god.rb'
        execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec god start'
      end
    end

    task :start_god do
      on roles(:app) do
        execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec god RAILS_ENV=staging -c /srv/apps/c4c-conversations/shared/config/production_god.rb'
      end
    end
  end

end
# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
# and/or per server
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
# setting per server overrides global ssh_options

# fetch(:default_env).merge!(rails_env: :production)
