set :application, 'c4c-conversations'
set :repo_url, 'git@github.com:idyllicsoftware/c4c-conversations.git'
set :branch, 'master'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :deploy_to, '/srv/apps/c4c-conversations'
set :scm, :git

set :format, :pretty
set :log_level, :debug
# set :pty, true

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}


set :rvm1_type, :user
set :rvm1_ruby_version, 'ruby-2.1.0@c4c-conversations'
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :keep_releases, 5
set :bundle_roles, :all                                  # this is default
set :bundle_servers, -> { release_roles(fetch(:bundle_roles)) } # this is default
set :bundle_gemfile, -> { release_path.join('Gemfile') }
#set :bundle_dir, -> { shared_path.join('bundle') }
set :bundle_flags, '--deployment'
set :bundle_without, %w{development test}.join(' ')
#set :bundle_binstubs, -> { shared_path.join('bin') }
set :bundle_roles, :all
#set :normalize_asset_timestamps, %{public/images public/javascripts
# public/stylesheets}

SSHKit.config.command_map[:rake]  = "bundle exec rake"
SSHKit.config.command_map[:rails]  = "bundle exec rails"

namespace :deploy do

  task :migrate do
    on roles(:app) do
      execute 'cd /srv/apps/c4c-conversations/current && /home/deploy/.rvm/bin/rvm ruby-2.1.0@c4c-conversations do bundle exec rake db:migrate RAILS_ENV=staging'
    end
  end

  #task :copy_god_files do
  #  on roles(:app) do
  #    execute 'cp /srv/apps/c4c-conversations/current/config/staging_god.rb /srv/apps/c4c-conversations/shared/config/.'
  #  end
  #end

  #task :restart_god do
  #  on roles(:app) do
  #    execute 'cd /srv/apps/c4c-conversations/current && /usr/local/bin/god stop'
  #    execute 'cd /srv/apps/c4c-conversations/current && /usr/local/bin/god -c /srv/apps/c4c-conversations/shared/config/staging_god.rb -D'
  #  end
  #end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end


  before :finishing, 'deploy:copy_god_files'
  after :finishing, 'deploy:cleanup'
  after :finished, 'deploy:restart_god'

end
