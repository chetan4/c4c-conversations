class ParentTweetWorker
  @queue = :parent_tweet_worker

  def self.perform(tweet_ids, api_request_id)
    tids = tweet_ids.split(",")
    tweets = Tweet.find(tids)
    Rails.logger.debug("Fetching parent tweets for #{tids}")
    api_request = ApiRequest.find(api_request_id)

    unless api_request.cancelled?
      parent_tweets = ::Gnip::ParentTweets.fetch_parent_tweets(tweets)
      api_request.increment_rehydration_counter!

      batch_id = parent_tweets.parent_tweets.first.batch_id
      tweets_with_replies = parent_tweets.reply_tweets
      Resque.enqueue(ConversationUpdaterWorker, batch_id, api_request_id)

      unless tweets_with_replies.empty?
        _tids = tweets_with_replies.collect {|x| x.id }
        Resque.enqueue(ParentTweetWorker, _tids.join(","), api_request_id)
      end
    end
  end
end