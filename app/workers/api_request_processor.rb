class ApiRequestProcessor
  @queue = :api_request_processor

  def self.perform(api_request_id)
    api_request = ApiRequest.find(api_request_id)

    unless api_request.cancelled?
      Gnip::Finder.new(api_request).get_tweets
    end
  end
end