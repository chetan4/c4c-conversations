class ConversationUpdaterWorker
  @queue = :conversation_updater_worker

  def self.perform(batch_id, api_request_id)
    Rails.logger.debug("Updating conversations....")
    Gnip::ConversationUpdater.new(batch_id, api_request_id).update!
  end
end