# == Schema Information
#
# Table name: tweet_conversations
#
#  id              :integer          not null, primary key
#  conversation_id :integer
#  tweet_id        :integer
#  position        :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_tweet_conversations_on_conversation_id  (conversation_id)
#  index_tweet_conversations_on_tweet_id         (tweet_id)
#

class TweetConversation < ActiveRecord::Base
  attr_accessible :conversation_id, :position, :tweet_id

  belongs_to :conversation
  belongs_to :tweet

  validates_presence_of :position, :tweet_id, :conversation_id

  default_scope order('position ASC')
end
