# == Schema Information
#
# Table name: twitter_users
#
#  id                 :integer          not null, primary key
#  link               :string(255)
#  display_name       :string(255)
#  image_url          :string(255)
#  summary            :text
#  follower_count     :integer          default(0)
#  statuses_count     :integer          default(0)
#  twitter_time_zone  :string(255)
#  preferred_username :string(255)
#  location           :string(255)
#  favorites_count    :integer          default(0)
#  other_links        :text
#  following_count    :integer          default(0)
#  entity_type        :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class TwitterUser < ActiveRecord::Base
  attr_accessible :display_name, :entity_type, :favorites_count, :follower_count,
                  :following_count, :friends_count, :image_url, :link,
                  :other_links, :location, :preferred_username,
                  :statuses_count, :summary, :twitter_time_zone

  validates_uniqueness_of :preferred_username

  has_many :tweets

  TwitterUser.include_root_in_json = false
end
