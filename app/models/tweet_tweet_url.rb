# == Schema Information
#
# Table name: tweet_tweet_urls
#
#  id           :integer          not null, primary key
#  tweet_id     :integer
#  tweet_url_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_tweet_tweet_urls_on_tweet_id_and_tweet_url_id  (tweet_id,tweet_url_id)
#

class TweetTweetUrl < ActiveRecord::Base
  attr_accessible :tweet_id, :tweet_url_id

  belongs_to :tweet
  belongs_to :tweet_url
end
