# == Schema Information
#
# Table name: conversations
#
#  id             :integer          not null, primary key
#  root_tweet_id  :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  checksum       :string(255)
#  api_request_id :integer
#
# Indexes
#
#  index_conversations_on_api_request_id  (api_request_id)
#  index_conversations_on_checksum        (checksum)
#

class Conversation < ActiveRecord::Base
  attr_accessible :root_tweet_id, :api_request_id

  belongs_to :api_request
  belongs_to :root_tweet, :class_name => 'Tweet', :foreign_key => 'root_tweet_id'
  has_many :tweet_conversations, :dependent => :destroy,:class_name => 'TweetConversation'
  has_many :tweets, :through => :tweet_conversations, :class_name => 'Tweet'

  validates_presence_of :root_tweet_id, :checksum

  def self.generate_checksum(tweets)
    tweet_ids = tweets.collect{|tw| tw.tweet_id}
    return '' if tweet_ids.empty?
    Digest::MD5.hexdigest(tweet_ids.sort.join("|"))
  end

  def append!(tweet)
    transaction do
      self.root_tweet_id = tweet.id
      self.tweet_conversations.each do |tc|
        tc.position += 1
        tc.save
      end

      tweet_conversation = self.tweet_conversations.build(:tweet_id => tweet.id,
                                                          :position => 1)

      tweet_conversation.save!
      publish_conversation
    end
  end

  #TODO: notify on failure of push!
  def publish_conversation
    #$redis.lpush(REDIS_CONVERSATIONS_FOR_HQ, self.as_json.to_json)
    Resque.push(:conversations_for_hq, :class => 'ConversationUpdaterForHq',
                :args => self.as_json.to_json)
  end

  def to_s
    self.tweets.each do |tweet|
      Rails.logger.debug("#{tweet.posted_at} #{tweet.tweeter.preferred_username}: #{tweet.body}")
      puts("#{tweet.posted_at} #{tweet.tweeter.preferred_username}: #{tweet.body}")
    end
  end

  def as_json
    api_request = ApiRequest.find(self.api_request_id)

    {
        :id => self.id,
        :root_tweet_id => self.root_tweet_id,
        :keywords => api_request.keywords,
        :api_request_id => api_request.request_id,
        :pagination => api_request.pagination,
        :tweets_count => self.tweets.count,
        :created_at => self.created_at,
        :updated_at => self.updated_at,
        :tweets => conversation_tweets
    }
  end

  def conversation_tweets
    self.tweets.includes(:hash_tags, :tweet_urls).reverse.map(&:as_json)
  end
end
