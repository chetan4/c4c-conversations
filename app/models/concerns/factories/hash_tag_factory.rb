module Factories
  class HashTagFactory
    def self.build(tag_text)
      HashTag.find_or_initialize_by_tag(tag_text)
    end
  end
end