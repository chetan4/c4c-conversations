module Factories
  class TweetUrlFactory
    def self.build(options = {})
      tweet_url = TweetUrl.find_or_initialize_by_expanded_url(options.expanded_url)
      tweet_url.short_url = options.short_url
      tweet_url
    end
  end
end