module Factories
  class TweetFactory
    extend Gnip::Util

    def self.build(options = {})
      tweet = Tweet.new
      tweet.body = options.body
      tweet.link = options.link
      tweet.posted_at = Time.parse(options.postedTime)
      tweet.retweet_count = options.retweetCount
      tweet.tweet_id = fetch_tweet_id(options.id)

      if options.inReplyTo.present?
        tweet.in_reply_to_tweet_id = fetch_in_reply_to_tweet_id(options.inReplyTo)
      end
      tweet
    end
  end
end