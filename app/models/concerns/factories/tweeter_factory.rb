module Factories
  class TweeterFactory
    def self.build(options = {})
      tweeter = TwitterUser.where(:preferred_username => options.preferredUsername).last
      return tweeter if tweeter.present?

      tweeter = TwitterUser.new
      tweeter.link = options.link
      tweeter.display_name = options.displayName
      tweeter.image_url = options.image
      tweeter.summary = options.summary
      tweeter.follower_count = options.followersCount
      tweeter.statuses_count = options.statusesCount
      tweeter.twitter_time_zone = options.twitterTimeZone
      tweeter.preferred_username = options.preferredUsername
      tweeter.location = options.location!.displayName
      tweeter.following_count = options.friendsCount
      tweeter.favorites_count = options.favoritesCount
      tweeter.entity_type = options.objectType
      tweeter.other_links = options.links.collect{|li| li['href'].to_s}.join(',')
      tweeter
    end
  end
end