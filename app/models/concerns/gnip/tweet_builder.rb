module Gnip
  class TweetBuilder
    include Gnip::Util

    attr_accessor :tweet, :tweeter, :hash_tags, :tweet_urls, :tweet_hash

    def self.prepare_tweet(tweet_hash, batch_id, batch_timestamp)
      builder = TweetBuilder.new(tweet_hash, batch_id, batch_timestamp)
      builder.populate_tweet
      builder.populate_tweeter
      builder.populate_hash_tags
      builder.populate_tweet_urls
      builder.tweet.tweeter = builder.tweeter
      builder.hash_tags.each {|ht| builder.tweet.hash_tags.push(ht)}
      builder.tweet_urls.each {|tu| builder.tweet.tweet_urls.push(tu) }
      builder.tweet
    end

    def initialize(tweet_hash, batch_id, batch_timestamp)
      begin
        @tweet_hash = tweet_hash
        @tweet = nil
        @tweeter = nil
        @hash_tags = []
        @tweet_urls = []
        @batch_id = batch_id
        @batch_timestamp = batch_timestamp
      rescue => e
        Rails.logger.error("[ERROR] #{e.message} \n  #{e.backtrace}")
      end
    end

    def populate_tweet
      tweet = Factories::TweetFactory.build(self.tweet_hash)
      tweet.batch_id = @batch_id
      tweet.batch_timestamp = @batch_timestamp

      self.tweet = tweet
    rescue => e
      Rails.logger.error("[ERROR] #{e.message} \n  #{e.backtrace}")
    end

    # Tweeter is the guy who tweeted out the content
    def populate_tweeter
      self.tweeter = Factories::TweeterFactory.build(self.tweet_hash.actor)
    end

    def populate_hash_tags
      self.tweet_hash.twitter_entities.hashtags.each do |ht|
        hash_tag = Factories::HashTagFactory.build(ht.text)
        self.hash_tags.push(hash_tag)
      end

      self.hash_tags
    end

    def populate_tweet_urls
      self.tweet_hash.twitter_entities.urls.each do |u|
        tweet_url = Factories::TweetUrlFactory.build(u)
        self.tweet_urls.push(tweet_url)
      end

      self.tweet_urls
    end
  end
end