module Gnip
  module Util
    def fetch_in_reply_to_tweet_id(reply_to_tweet_url)
      link = reply_to_tweet_url.link
      link.split("/").last
    end

    def fetch_tweet_id(id)
      id.split(':').last.to_s
    end

    def self.generate_unique_token(batch_timestamp)
      uuid = SecureRandom.uuid
      (uuid + "-#{batch_timestamp.to_i}")
    end
  end
end