module Gnip
  class ConversationUpdater
    attr_accessor :tweets, :api_request_id

    def initialize(batch_id, api_request_id)
      @tweets = Tweet.where(:batch_id => batch_id)
      @api_request_id = api_request_id
    end

    def update!
      self.tweets.each do |tweet|
        if tweet.has_a_response?
          response_tweets = tweet.response_tweets

          response_tweets.each do |response_tweet|

            if response_tweet.is_part_of_a_conversation?
              conversations = response_tweet.fetch_conversations

              conversations.each do |conversation|
                conversation.append!(tweet)
              end
            else
              Gnip::ConversationBuilder.new(response_tweet, @api_request_id).build_conversation
            end # end of if
          end # end of do
        end # end of if
      end
    end

  end
end