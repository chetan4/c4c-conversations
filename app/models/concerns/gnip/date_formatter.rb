module Gnip
  module DateFormatter
    def self.format(date)
      Time.parse(date.to_s).strftime("%Y%m%d%H%M")
    end
  end
end