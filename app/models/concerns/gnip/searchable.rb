require 'gnip_api'

module Gnip
  class Searchable
    include ActiveModel::Validations
    include Gnip::Constants

    attr_accessor :keyword, :from_date, :to_date, :search_response, :bucket,
                  :request, :pagination

    # Returns an Array of <Tweet>
    def self.tweets(keyword, from_date = (Date.today - 1.month), to_date = Date.today, options = {})
      searchable = Searchable.new(keyword, from_date, to_date, options)
      searchable.search
      searchable.parse_response
    end

    def initialize(keyword, from_date = (Date.today - 1.month), to_date = Date.today, options = {})
      @keyword = keyword
      @from_date = Gnip::DateFormatter.format(from_date)
      @to_date = Gnip::DateFormatter.format(to_date)
      @bucket = options[:bucket] || 'day'
    end

    def tweets(pagination_string = nil)
      search(pagination_string)
      parse_response
    end

    #TODO: Handle exceptions thrown by Gnip API gem in the controller
    # Errors include
    # 1. GnipApi::General
    # 2. GnipApi::Unauthorized
    # 3. GnipApi::NotFound
    # 4. GnipApi::InformGnip
    # 5. GnipApi::Unavailable
    #####################################################################
    def search(pagination_string = nil)
      @request = GnipApi::Search.new(SEARCH_API_URL)
      query_hash = {:query => self.keyword,
                    :publisher => DEFAULT_PUBLISHER,
                    :maxResults => MAX_SEARCH_RESULTS,
                    :fromDate => self.from_date,
                    :toDate => self.to_date }

      query_hash.merge!({:next => pagination_string}) if pagination_string.present?

      @request.search(query_hash)
    end

    def parse_response
      if @request.response.success?
        response_body = @request.response.body
        @pagination = parse_for_pagination_string(response_body)
        Gnip::JsonToTweet.tweet_with_replies(response_body)
      else
        raise 'Request could not be accepted'
      end
    end

    def parse_for_pagination_string(response_body)
       res = Hashie::Mash.new(JSON.parse(response_body))
       res.next
    end
  end
end