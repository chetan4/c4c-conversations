module Gnip
  module Constants
    SEARCH_API_URL = 'https://search.gnip.com/accounts/IdyllicSoftware/search/prod.json'
    DEFAULT_PUBLISHER = 'twitter'
    MAX_SEARCH_RESULTS = 500
    REHYDRATION_API_URL = 'https://rehydration.gnip.com:443/accounts/IdyllicSoftware/publishers/twitter/rehydration/activities.json'
  end
end