module Gnip
  class ConversationBuilder
    attr_accessor :tweet, :conversation, :conversation_tweets
    def initialize(tweet, api_request_id)
      @tweet = tweet
      @conversation = Conversation.new(:api_request_id => api_request_id)
      @conversation_tweets = []
      @conversation_tweets.push(@tweet)
    end

    def build(tweet)
      if ::Tweet.has_parent_tweet?(tweet)
        parent_tweet = ::Tweet.fetch_parent_tweet(tweet)
        self.conversation_tweets.push(parent_tweet)
        build(parent_tweet) if ::Tweet.has_parent_tweet?(parent_tweet)
      end

      return self.conversation_tweets
    end

    def build_conversation
      ActiveRecord::Base.transaction do
        build(self.tweet)
        save_conversations!
      end
    end

    def save_conversations!
      return if self.conversation_tweets.count < 2

      root_tweet_id = self.conversation_tweets.last.id
      @conversation.root_tweet_id = root_tweet_id
      @conversation.checksum = Conversation.generate_checksum(self.conversation_tweets)
      @conversation.save!

      self.conversation_tweets.each_with_index do |ct, index|
        conversation_tweet = TweetConversation.new(:position => index,
                                                   :tweet_id => ct.id,
                                                   :conversation_id => @conversation.id)
        conversation_tweet.save!
      end

      @conversation.publish_conversation
    end
  end
end