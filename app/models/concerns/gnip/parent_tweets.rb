module Gnip
  class ParentTweets
    attr_accessor :tweets, :request, :parent_tweets

    # Accepts Tweets as an Array of <Tweet> objects
    # Paramters
    # tweets: Array[Tweet]
    #
    # Response:
    # tweets: Array[Tweet]
    # Array of Tweets

    def self.tweet_with_replies(tweets)
      pt = ParentTweets.new(tweets)
      pt.fetch_tweets
      pt.parse_response
      pt.reply_tweets
    end

    def self.fetch_parent_tweets(tweets)
       parent_tweets = ParentTweets.new(tweets)
       parent_tweets.fetch_tweets
       parent_tweets.parse_response
       parent_tweets
    end

    def initialize(tweets)
      @tweets = tweets
      @tweet_ids = @tweets.collect {|t| t.in_reply_to_tweet_id }
      @parent_tweets = []
    end

    # Fetches parent tweets for all the listed tweet_ids in the
    # <tt> @tweet_ids </tt> variable
    def fetch_tweets
      @request = GnipApi::Rehydration.new(Gnip::Constants::REHYDRATION_API_URL)
      @request.get_tweets(@tweet_ids)
    end

    def parse_response
      if @request.response.success?
        response_body = @request.response.body
        response_tweets_array = JSON.parse(response_body)
        parse_tweets(response_tweets_array)

        self.parent_tweets
      else
        []
      end
    end

    def parse_tweets(response_tweets_array)
      batch_timestamp = DateTime.now
      batch_id = Util.generate_unique_token(batch_timestamp)

      response_tweets_array.each do |tweet|
        tweet_hash = Hashie::Mash.new(tweet)
        next unless tweet_hash.available?
        tweet_object = Gnip::TweetBuilder.prepare_tweet(tweet_hash.content,
                                                        batch_id,
                                                        batch_timestamp)
        tweeter = tweet_object.tweeter

        begin
          save_and_update_tweets!(tweeter, tweet_object)
        rescue ActiveRecord::RecordInvalid => e
          next
        rescue => e
          Rails.logger.error("[ERROR] #{e.message} \n #{e.backtrace}")
          next
        end
      end
    end

    def save_and_update_tweets!(twitter_user, tweet_object)
      ActiveRecord::Base.transaction do
        twitter_user.save! if twitter_user.new_record?
        tweet_object.twitter_user_id = twitter_user.id
        tweet_object.save!
      end

      self.parent_tweets.push(tweet_object)
    end


    def reply_tweets
      self.parent_tweets.select{|t| !t.in_reply_to_tweet_id.nil? }
    end
  end
end