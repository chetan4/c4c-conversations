module Gnip
  class JsonToTweet

    attr_accessor :json_string, :tweets_hash, :tweets, :batch_id,
                  :batch_timestamp

    # Return with replies
    # ====================
    # Returns all tweets with which are replies to another tweet.

    def self.tweet_with_replies(json_string)
      json_to_tweet = JsonToTweet.new(json_string)
      json_to_tweet.to_tweets
      json_to_tweet.reply_tweets
    end

    # Json to tweet API
    # ===================
    # Converts a json string containing an array of tweets into Tweet objects
    # All the tweets fetched in the process are saved in the database.
    # Returns an array containing <Tweet> objects

    def self.convert(json_string)
      json_to_tweet = JsonToTweet.new(json_string)
      json_to_tweet.to_tweets
    end

    def initialize(json_string)
      @json_string = json_string
      @batch_timestamp = DateTime.now
      @batch_id = Gnip::Util.generate_unique_token(@batch_timestamp)
      @tweets_hash = []
      @tweets = []
    end

    def to_hash
      tweets_hash = JSON.parse(json_string)
      self.tweets_hash = tweets_hash['results'] if tweets_hash.has_key?('results')
    end

    # to_tweet.
    # public api method
    #
    # Converts a hash of tweets stored in the tweet_hash to an array of Tweet
    # objects
    #
    # <tt> @tweet_hash </tt> is an array of hashes with each hash representing
    # a tweet
    # <tt> @tweets </tt> is an array of Tweet objects
    def to_tweets
      to_hash

      return [] if self.tweets_hash.empty?
      self.tweets_hash.each do |tweet|
        twt = Hashie::Mash.new(tweet)
        tweet_object = Gnip::TweetBuilder.prepare_tweet(twt,
                                                        self.batch_id,
                                                        self.batch_timestamp)
        twitter_user = tweet_object.tweeter
        begin
          save_and_update_tweets!(twitter_user, tweet_object)
        rescue ActiveRecord::RecordInvalid => e
          next
        rescue => e
          Rails.logger.error("[ERROR] #{e.message} \n #{e.backtrace}")
          next
        end
      end
    end

    # Saves a twitter_user and the associated tweet to the database
    # On successful save it updates the <tt> @tweets </tt> array with the
    # newly added tweet.
    def save_and_update_tweets!(twitter_user, tweet_object)
      ActiveRecord::Base.transaction do
        twitter_user.save!
        tweet_object.twitter_user_id = twitter_user.id
        tweet_object.save!
      end

      self.tweets.push(tweet_object)
    end

    # Returns from @tweets all the tweet objects that have an
    # in_reply_to_tweet_id
    #
    # Tweets which respond to inReplyTo are a response to a specific tweet.
    #
    def reply_tweets
      self.tweets.select{|t| !t.in_reply_to_tweet_id.nil? }
    end
  end
end