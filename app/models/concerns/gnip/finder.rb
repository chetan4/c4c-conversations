module Gnip
  class Finder
    attr_accessor :api_request, :keywords, :start_date, :end_date, :bucket

    def self.api_request
      api_request = ::ApiRequest.new()
      api_request.keywords = 'Eich, Mozilla'
      api_request.start_date = DateTime.now - 1.month
      api_request.end_date = DateTime.now
      api_request.publisher = 'twitter'
      api_request.bucket = 'day'
      api_request
    end

    def initialize(api_request)
      @api_request = api_request
      @keywords = api_request.keywords
      @start_date = api_request.start_date
      @end_date = api_request.end_date
      @bucket = api_request.bucket || 'day'
    end

    def get_tweets
      searchable = Searchable.new(self.keywords,
                                  self.start_date,
                                  self.end_date,
                                  {:bucket => self.bucket})

      tweet_with_replies = searchable.tweets(self.api_request.pagination)
      self.api_request.update_attributes({:pagination => searchable.pagination})
      self.api_request.increment_search_counter!
      unless tweet_with_replies.empty?
        parent_tweets = ParentTweets.fetch_parent_tweets(tweet_with_replies)
        parent_tweet_with_replies = parent_tweets.reply_tweets #tweets that are a reply to another

        self.api_request.increment_rehydration_counter!

        unless parent_tweet_with_replies.empty?
          tids = parent_tweet_with_replies.collect{|x|x.id }.join(",")
          Resque.enqueue(ParentTweetWorker, tids, @api_request.id)
        end

        if parent_tweets.parent_tweets.present?
          batch_id = parent_tweets.parent_tweets.first.batch_id
          Gnip::ConversationUpdater.new(batch_id, @api_request.id).update!
        end
      end
    end
  end
end