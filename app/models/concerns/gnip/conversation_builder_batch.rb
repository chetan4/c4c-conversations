module Gnip
  class ConversationBuilderBatch

    attr_accessor :tweets, :batch_id, :conversations
     def initialize(tweet_batch_id)
       @tweet_batch_id = tweet_batch_id
       @tweets = Tweet.where(:batch_id => tweet_batch_id)
       @conversations = []  # An Array of conversation objects
     end

     def get_conversations
       self.tweets.each do |tweet|
          conversation_builder = Gnip::ConversationBuilder.new(tweet)
          conversation_builder.build_conversation
          self.conversations.push(conversation_builder.conversation)
       end

       self.conversations
     end
  end
end