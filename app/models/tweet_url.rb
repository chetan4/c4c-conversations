# == Schema Information
#
# Table name: tweet_urls
#
#  id           :integer          not null, primary key
#  short_url    :string(255)
#  expanded_url :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class TweetUrl < ActiveRecord::Base
  attr_accessible :expanded_url, :short_url

  has_many :tweet_tweet_urls, :dependent => :destroy
  has_many :tweets, :through => :tweet_tweet_urls, :class_name => 'Tweet'

  TweetUrl.include_root_in_json = false
end
