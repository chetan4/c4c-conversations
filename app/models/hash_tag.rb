# == Schema Information
#
# Table name: hash_tags
#
#  id         :integer          not null, primary key
#  tag        :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class HashTag < ActiveRecord::Base
  attr_accessible :tag

  has_many :tweet_hash_tags, :dependent => :destroy
  has_many :tweets, :through => :tweet_hash_tags, :class_name => 'Tweet'

  HashTag.include_root_in_json = false
end
