# == Schema Information
#
# Table name: api_requests
#
#  id                        :integer          not null, primary key
#  keywords                  :text
#  api_response_id           :integer
#  start_date                :datetime
#  end_date                  :datetime
#  bucket                    :string(255)
#  publisher                 :string(255)
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  request_id                :string(255)
#  request_type              :string(255)
#  search_request_count      :integer          default(0)
#  rehydration_request_count :integer          default(0)
#  status                    :string(255)      default("REQUESTED")
#  pagination                :text
#
# Indexes
#
#  index_api_requests_on_api_response_id  (api_response_id)
#  index_api_requests_on_request_id       (request_id)
#

class ApiRequest < ActiveRecord::Base
  attr_accessible :bucket, :end_date, :keywords, :publisher, :request_id,
                  :start_date, :pagination

  REQUEST_STATUS_REQUESTED = 'REQUESTED'
  REQUEST_STATUS_CANCELLED = 'CANCELLED'

  has_many :conversations

  def execute
    Resque.enqueue(ApiRequestProcessor, self.id)
  end

  def increment_search_counter!
    self.increment!(:search_request_count)
  end

  def increment_rehydration_counter!
    self.increment!(:rehydration_request_count)
  end

  def cancelled?
    self.status == REQUEST_STATUS_CANCELLED
  end

  def cancel_request!
    cancel_request
    self.save!
  end

  def cancel_request
    self.status == REQUEST_STATUS_CANCELLED
  end
end
