# == Schema Information
#
# Table name: tweets
#
#  id                   :integer          not null, primary key
#  body                 :string(255)
#  link                 :string(255)
#  posted_at            :datetime
#  favorites_count      :integer          default(0)
#  retweet_count        :integer          default(0)
#  twitter_user_id      :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  batch_id             :string(255)
#  batch_timestamp      :datetime
#  tweet_id             :string(255)
#  in_reply_to_tweet_id :string(255)
#
# Indexes
#
#  index_tweets_on_batch_id              (batch_id)
#  index_tweets_on_batch_timestamp       (batch_timestamp)
#  index_tweets_on_in_reply_to_tweet_id  (in_reply_to_tweet_id)
#  index_tweets_on_tweet_id              (tweet_id)
#

class Tweet < ActiveRecord::Base
  attr_accessible :body, :favorites_count, :link,
                  :posted_at, :retweet_count, :twitter_user_id,
                  :batch_id, :batch_timestamp

  validates_presence_of :body, :favorites_count, :retweet_count,
                        :twitter_user_id, :link, :tweet_id
  validates_uniqueness_of :tweet_id, :link

  has_many :tweet_hash_tags, :dependent => :destroy
  has_many :hash_tags, :through => :tweet_hash_tags
  belongs_to :tweeter, :class_name => 'TwitterUser', :foreign_key => 'twitter_user_id'
  has_many :tweet_tweet_urls, :dependent => :destroy
  has_many :tweet_urls, :through => :tweet_tweet_urls

  Tweet.include_root_in_json = false

  def self.fetch_parent_tweet(tweet)
    Tweet.where(:tweet_id => tweet.in_reply_to_tweet_id).last
  end

  def self.has_parent_tweet?(tweet)
    fetch_parent_tweet(tweet).nil? ? false : true
  end

  def has_a_response?
    response_tweets.present?
  end

  def response_tweets
    Tweet.where(:in_reply_to_tweet_id => self.tweet_id)
  end

  def is_part_of_a_conversation?
    fetch_conversations.present?
  end

  def fetch_conversations
    TweetConversation.where(:tweet_id => self.id).map{ |x| x.conversation }
  end

  def as_json
    {
        :id => self.id,
        :tweet_id => self.tweet_id,
        :body => self.body,
        :link => self.link,
        :posted_at => self.posted_at,
        :favorites_count => self.favorites_count,
        :retweet_count => self.retweet_count,
        :twitter_user => self.tweeter.as_json,
        :tweet_urls => self.tweet_urls.map(&:as_json),
        :hash_tags => self.hash_tags.map(&:as_json)
    }
  end
end
