# == Schema Information
#
# Table name: tweet_hash_tags
#
#  id          :integer          not null, primary key
#  hash_tag_id :integer
#  tweet_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_tweet_hash_tags_on_hash_tag_id_and_tweet_id  (hash_tag_id,tweet_id)
#

class TweetHashTag < ActiveRecord::Base
  attr_accessible :tag_id, :tweet_id

  belongs_to :tweet
  belongs_to :hash_tag
end
