class Api::ConversationsController < ApplicationController
  protect_from_forgery :only => []
  def index
  end

  def create
    @keywords = params[:keywords]
    @request_id = params[:request_id]
    @api_request = prepare_api_request

    if @api_request.save
      @api_request.execute
      render :json => {:request_status => :accepted},
             :status => :accepted
    else
      render :json => {:request_status => :bad_request},
             :status => :bad_request
    end
  rescue => e
    Rails.logger.error("#{e.message} #{e.backtrace}")
    render :json => {:request_status => :internal_server_error},
           :status => :internal_server_error
  end

  def show
  end

  # DELETE conversations/:api_request_id
  def destroy
    @api_request = ApiRequest.where(:request_id => params[:api_request_id]).last

    if !@api_request.present?
      render :json => {:request_status => :bad_request}, :status => :bad_request
    else
      if @api_request.cancel_request!
        render :json => {:request_status => :accepted}, :status => :accepted
      else
        render :json => {:request_status => 'Request Could not be processed at
                          this moment.'}, :status => :ok
      end
    end
  end

  private

  def prepare_api_request
    api_request = ApiRequest.find_or_initialize_by_request_id(params[:request_id])

    if api_request.new_record?
      api_request.keywords = params[:keywords]
      api_request.start_date = DateTime.now - 1.month unless params[:start_date].present?
      api_request.end_date = DateTime.now unless params[:end_date].present?
      api_request.publisher = 'twitter' unless params[:publisher].present?
      api_request
    else
      create_updated_api_request(api_request)
    end
  end

  def create_updated_api_request(old_request)
    api_request = ApiRequest.new(:request_id => params[:request_id],
                                 :keywords => params[:keywords],
                                 :publisher => 'twitter'
                                )

    if old_request.end_date.to_date < Date.today
      api_request.start_date = old_request.end_date
      api_request.end_date = DateTime.now
    else
      api_request.start_date = old_request.start_date
      api_request.end_date = old_request.end_date
      api_request.pagination = params[:pagination_string]
    end

    api_request
  end
end
